/*
  Universidad de Costa Rica
  Estructuras de computadoras digitales II

  Proyecto final II-2019

  Creado por Jorge Munoz Taylor
*/

#ifndef _PRIMES_H_
#define _PRIMES_H_

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <ctype.h>

/*
    Return type.
 */
enum function_type
{
    OK,
    ERROR,
    PARAM
};

/*
    Number of threads and the sweep max value
 */
struct parameters
{
    int N;
    int number_of_threads;
};

/*
 * Receive the parameters from the terminal, the number of threads and the search range.
 * 
 * [in] argc: number of user args.
 * [in] argv: user args.
 * [out] PARAMETERS: struct that stores the user args.
*/
int parse_args( int argc, char **argv, parameters *PARAMETERS);

/*
 * Calculate the program time, start the loop for find the primes.
 * 
 * [in] min_val: min value of the sweep.
 * [in] max_val: max value of the sweep.
 * [in] FACTOR: The higher the FACTOR is, the more numbers will be analyzed.
 */
int prime_sweep ( int min_val, int max_val, int FACTOR );

/*
 * Function that calculate the prime numbers.
 * 
 * [in] n: actual max value.
 */
int prime_calc ( int n );

#endif