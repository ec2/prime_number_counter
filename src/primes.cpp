/*
  Universidad de Costa Rica
  Estructuras de computadoras digitales II

  Proyecto final II-2019

  Creado por Jorge Munoz Taylor
*/

#include <primes.h>

using namespace std;


int parse_args( int argc, char **argv, parameters *PARAMETERS)
{
    if( argc == 3 )
    {
        if( isdigit( *argv[1] ) != 0 ) PARAMETERS->N = atoi(argv[1]);
        else return PARAM;

        if( isdigit( *argv[2] ) != 0 ) PARAMETERS->number_of_threads = atoi(argv[2]); 
        else return PARAM;
    }
    else return PARAM;

    return OK;
}/* End of function */



int prime_sweep ( int min_val, int max_val, int FACTOR )
{
  int    n;
  int    primes;
  double wtime;

  if( min_val < 1 ) return ERROR;
  if( max_val < 1 ) return ERROR; 

  printf ( "\n" );
  printf ( "  Prime number count from 1 to %d.\n", max_val );
  printf ( "\n" );

  n = min_val;

  wtime = omp_get_wtime ( );
  
  while ( n <= max_val )
  {
    primes = prime_calc ( n );

    n = n*FACTOR; 
  }/* While end */

  wtime = omp_get_wtime ( ) - wtime;

  printf ( "    --> N:            %d\n\n" , max_val );
  printf ( "    --> Primes found: %d\n\n" , primes  );
  printf ( "    --> Time:          %f seconds\n", wtime   );
 
  return OK;
}/* end of function */



int prime_calc ( int n )
{
  int i;
  int j;
  int prime;
  int total = 0;

/* Call the API for define the shared and private parameters */
# pragma omp parallel \
  shared ( n ) \
  private ( i, j, prime )
  

/* Here is the definition of the Eratostenes Sieve*/
# pragma omp for reduction ( + : total )
  for ( i = 2; i <= n; i++ )
  {
    prime = 1;

    for ( j = 2; j < i; j++ )
    {
      if ( i % j == 0 )
      {
        prime = 0;
        break;
      }
    }/* for end */
    total = total + prime;
  }/* for end */

  return total;
}/* end of function */