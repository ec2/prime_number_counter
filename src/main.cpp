/*
  Universidad de Costa Rica
  Estructuras de computadoras digitales II

  Proyecto final II-2019

  Creado por Jorge Munoz Taylor
*/

#include <stdlib.h>
#include <stdio.h>
#include <primes.h>
#include <omp.h>

/* Start value */
#define MIN    1
#define FACTOR 2

using namespace std;

int main ( int argc, char **argv )
{
  parameters PARAMETERS;

  /* Receive and verify the user args */
  if ( parse_args(argc, argv, &PARAMETERS) == PARAM )
  {
    printf("\nThere is an error in the parameters!\n\n");
    return PARAM;
  }

  printf ( "\n" );
  printf ( "  Number of processors available = %d\n", omp_get_num_procs ( ) );
  printf ( "  Number of threads available    = %d\n", omp_get_max_threads ( ) );

  /* Set the number of threads for the program */
  omp_set_num_threads( PARAMETERS.number_of_threads ); 
  printf ( "\n    --> Threads used = %d\n", PARAMETERS.number_of_threads );
  
  /* Call the sweep and verify if there is a range error */
  if ( prime_sweep ( MIN, PARAMETERS.N, FACTOR ) == ERROR )
  {
    printf("\n  Min or max values out of range!\n\n ");
    return ERROR;
  }
  
  /* Program end OK */
  printf ( "\n  Normal end of execution.\n\n" );

  return OK;
}/* end of main */