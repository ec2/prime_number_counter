# Cache simulator IE521 project baseline

Code base line for IE0521 final project

## Repository
Use git for clone the repository to the local machine
```
https://gitlab.com/ec2/prime_number_counter.git
```

## How to build the project
Create a build directory and run all targets there
```
>> mkdir build
>> cd build (root directory)
>> cmake ./
>> make 
```

## How to run the simulation
The simulation executable is located inside the build directory (src/prime)
```
./src/prime <Max number to find primes> <Number of threads>
```

## Dependencies
Make sure OpenMP and CMake are installed:
```
sudo apt-get install libomp-dev

sudo apt-get install cmake 
```

## Authors
```
Jorge Muñoz Taylor

Douglas González Parra
```
